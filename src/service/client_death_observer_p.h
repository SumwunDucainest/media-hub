/*
 * Copyright © 2021-2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_MEDIAHUBSERVICE_CLIENT_DEATH_OBSERVER_PRIVATE_H
#define LOMIRI_MEDIAHUBSERVICE_CLIENT_DEATH_OBSERVER_PRIVATE_H

#include "client_death_observer.h"

namespace lomiri {
namespace MediaHubService {

class ClientDeathObserverPrivate
{
    Q_DECLARE_PUBLIC(ClientDeathObserver)

public:
    ClientDeathObserverPrivate(ClientDeathObserver *q):
        q_ptr(q)
    {
    }
    virtual ~ClientDeathObserverPrivate() = default;

    virtual void registerForDeathNotifications(const Player::Client &client) = 0;

protected:
    void notifyClientDeath(const Player::Client &client) {
        Q_Q(ClientDeathObserver);
        Q_EMIT q->clientDied(client);
    }

private:
    ClientDeathObserver *q_ptr;
};

}} // namespace

#endif
