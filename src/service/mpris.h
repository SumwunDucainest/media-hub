/*
 * Copyright © 2021-2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MPRIS_H
#define MPRIS_H

#include <string>
#include <tuple>
#include <vector>

#include <cstdint>

namespace mpris {

struct Service
{
    static const QString &name()
    {
        static const QString s{"com.lomiri.MediaHub.Service"};
        return s;
    }

    struct Errors
    {
        struct CreatingSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.CreatingSession"
                };
                return s;
            }
        };

        struct DetachingSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.DetachingSession"
                };
                return s;
            }
        };

        struct ReattachingSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.ReattachingSession"
                };
                return s;
            }
        };

        struct DestroyingSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.DestroyingSession"
                };
                return s;
            }
        };

        struct CreatingFixedSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.CreatingFixedSession"
                };
                return s;
            }
        };

        struct ResumingSession
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.ResumingSession"
                };
                return s;
            }
        };

        struct PlayerKeyNotFound
        {
            static const QString &name()
            {
                static const QString s
                {
                    "com.lomiri.MediaHub.Service.Error.PlayerKeyNotFound"
                };
                return s;
            }
        };
    };

};

struct Player
{
    static const QString &name()
    {
        static const QString s{"org.mpris.MediaPlayer2.Player"};
        return s;
    }

    struct LoopStatus
    {
        LoopStatus() = delete;

        static const char* from(lomiri::MediaHubService::Player::LoopStatus status)
        {
            switch(status)
            {
            case lomiri::MediaHubService::Player::LoopStatus::none:
                return LoopStatus::none;
            case lomiri::MediaHubService::Player::LoopStatus::track:
                return LoopStatus::track;
            case lomiri::MediaHubService::Player::LoopStatus::playlist:
                return LoopStatus::playlist;
            }

            return nullptr;
        }

        static constexpr const char* none{"None"};
        static constexpr const char* track{"Track"};
        static constexpr const char* playlist{"Playlist"};
    };

    struct PlaybackStatus
    {
        PlaybackStatus() = delete;

        static const char* from(lomiri::MediaHubService::Player::PlaybackStatus status)
        {
            switch(status)
            {
            case lomiri::MediaHubService::Player::PlaybackStatus::null:
            case lomiri::MediaHubService::Player::PlaybackStatus::ready:
            case lomiri::MediaHubService::Player::PlaybackStatus::stopped:
                return PlaybackStatus::stopped;

            case lomiri::MediaHubService::Player::PlaybackStatus::playing:
                return PlaybackStatus::playing;
            case lomiri::MediaHubService::Player::PlaybackStatus::paused:
                return PlaybackStatus::paused;
            }

            return nullptr;
        }

        static constexpr const char* playing{"Playing"};
        static constexpr const char* paused{"Paused"};
        static constexpr const char* stopped{"Stopped"};
    };

    struct Error
    {
        struct OutOfProcessBufferStreamingNotSupported
        {
            static constexpr const char* name
            {
                "mpris.Player.Error.OutOfProcessBufferStreamingNotSupported"
            };
        };

        struct InsufficientAppArmorPermissions
        {
            static constexpr const char* name
            {
                "mpris.Player.Error.InsufficientAppArmorPermissions"
            };
        };

        struct UriNotFound
        {
            static constexpr const char* name
            {
                "mpris.Player.Error.UriNotFound"
            };
        };
    };
};

struct TrackList
{
    static const std::string& name()
    {
        static const std::string s{"org.mpris.MediaPlayer2.TrackList"};
        return s;
    }

    struct Error
    {
        struct InsufficientPermissionsToAddTrack
        {
            static constexpr const char* name
            {
                "mpris.TrackList.Error.InsufficientPermissionsToAddTrack"
            };
        };

        struct FailedToMoveTrack
        {
            static constexpr const char* name
            {
                "mpris.TrackList.Error.FailedToMoveTrack"
            };
        };

        struct FailedToFindMoveTrackSource
        {
            static constexpr const char* name
            {
                "mpris.TrackList.Error.FailedToFindMoveTrackSource"
            };
        };

        struct FailedToFindMoveTrackDest
        {
            static constexpr const char* name
            {
                "mpris.TrackList.Error.FailedToFindMoveTrackDest"
            };
        };

        struct TrackNotFound
        {
            static constexpr const char* name
            {
                "mpris.TrackList.Error.TrackNotFound"
            };
        };
    };
};

}

#endif // MPRIS_PLAYER_H
